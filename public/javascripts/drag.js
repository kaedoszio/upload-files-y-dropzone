
import {fileList,change_fileList} from './indes.js';

export const startDrag = function(element){
  
  let padre = element.querySelector('.dragItem');

  // [].forEach.call(padre,function(pa) {

  padre.addEventListener('dragover',events,false)
  padre.addEventListener('drop', droping, false);
  // });

  function events(e){
    e.preventDefault();
  }

  let img = padre.querySelector('.images-loader');
  

  // [].forEach.call(imgs,function(img) {
  // var imgClone = img.cloneNode(true);
  // img.parentNode.replaceChild(imgClone,img);

  img.addEventListener('dragstart',drag,false);
  img.addEventListener('click',function(e){ 
  e.stopPropagation();
  })

  // }); 

  


  function drag(ev) {
  ev.dataTransfer.setData("src", ev.target.id);
  }
 


  function droping(ev) {
    ev.preventDefault();

    if(document.getElementById(ev.dataTransfer.getData("src")).id == ev.currentTarget.firstElementChild.id){
      return
    }

      new Promise((resolve,reject)=>{

      var src = document.getElementById(ev.dataTransfer.getData("src"));
  
      var srcParent = src.parentNode; //image-options-que se envia
       
      var tgt = ev.currentTarget.firstElementChild; //image option objetivo

      // if(src.id == tgt.id){
      //   return reject('Drop sobre elemento existente')
      // }else{
        
        ev.currentTarget.replaceChild(src, tgt);
      
        let pos_insert = fileList.map((e)=>{return e.name}).indexOf(`${src.id}`);
        let pos_recive = fileList.map((e)=>{return e.name}).indexOf(`${tgt.id}`);
  
        srcParent.appendChild(tgt);
  
         resolve({pos_insert,pos_recive});
      // }
    })
    .then(async (obj)=>{
    await change_fileList(obj.pos_insert,obj.pos_recive);
    })
    .catch(e=>console.error(e))


  }



  let btn_borrar = element.querySelector('.boton-borrar').querySelector('.delete');

  // [].forEach.call(btn_borrar,function(btn){
  
    // var btnClone =  btn.cloneNode(true);
    // btn.parentNode.replaceChild(btnClone,btn);
  
   btn_borrar.addEventListener('click',borrarFoto,false)
  // })
  
  function borrarFoto(ev) {
    // ev.preventDefault();
    ev.stopPropagation();
    console.log(ev);
    
    var pos = fileList.map((e)=>{return e.name}).indexOf(`${ev.srcElement.parentNode.nextSibling.children[0].id}`)
    
    fileList.splice(pos,1)


    let e = ev.srcElement.parentNode.nextSibling.id;
    console.log(e);
    let padre = document.getElementById(e).parentNode
    
  
    padre.parentNode.removeChild(padre);

  }
}

// export startDrag;
