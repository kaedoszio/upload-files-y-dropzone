import {startDrag} from './drag.js';

export let fileList = [];

export let change_fileList = function (a,b){
    return [fileList[b],fileList[a]]=[fileList[a],fileList[b]];
}


window.onload = async function () {
  var drop = document.querySelector('.drop');
  // var form = document.querySelector('.box');
  //Check File API support
  if (window.File && window.FileList && window.FileReader) {

    var filesInput = document.createElement('input');

    filesInput.setAttribute('type', 'file');
    filesInput.setAttribute('multiple', true);
    filesInput.setAttribute('id', 'files');
    filesInput.setAttribute('accept', 'image/*');
    // filesInput.setAttribute('name', 'files');
    filesInput.style.display = 'none';

    drop.appendChild(filesInput);

    filesInput.addEventListener("change",input,false);

    drop.addEventListener('dragover',function(e){
      e.preventDefault();
      e.stopPropagation();
      drop.classList.add('dragover');
    })

    drop.addEventListener('dragleave',function(e){
      e.preventDefault();
      e.stopPropagation();
      drop.classList.remove('dragover');
    })

    drop.addEventListener('drop',function(e){
      e.preventDefault();
      e.stopPropagation();
      drop.classList.remove('dragover');
      input(e)
    })

    drop.addEventListener('click',function(e){
      //  e.preventDefault();
      // filesInput.value = null;
      filesInput.click();
    })

    function input(e) {

    document.getElementById('box_input').style.display ='none';
 
    let output = document.getElementById("form");

     let files;
      if(e.dataTransfer) {
        files = e.dataTransfer.files;
      } else if(e.target) {
        files = e.target.files;
      }
      


      for(let i = files.length - 1; i >= 0; i--){
        if(fileList.length >= 7)
        continue;
        fileList.push(files[i]);
      }
      


      asyncForEach(fileList, (file,index)=>{

        if(document.querySelector(`#item-${index}`)){return;}

        let divprueba = document.createElement('div');
        divprueba.setAttribute(`id`,`item-${index}`)
        divprueba.innerHTML = `<div class="boton-borrar">
        <button type="button" class="delete" name="button">X</button>
        </div>`;

        output.insertBefore(divprueba,null);

          readfile(file,async function(e){
          
          await new Promise ((resolve,reject)=>{
            e.srcElement.result ? resolve(e.srcElement.result) : reject('Imagen no fue cargada');
          })

          .then(src=>{
              // await new Promise ((resolve,reject)=>{
              var div = document.createElement("div");
              div.setAttribute(`id`,`dragItem-${index}`);
              div.setAttribute("draggable", "true");
              div.classList.add("dragItem")

              let image = new Image();
              image.setAttribute(`id`,`${file.name}`);
              image.setAttribute(`src`,`${src}`)
              image.setAttribute("class", "images-loader");
              
              image.setAttribute("draggable", "true");
              image.setAttribute("alt", `${file.name}`);

              // image.onload =async()=>{
              div.appendChild(image);
              document.querySelector(`#item-${index}`).appendChild(div);
              startDrag(document.querySelector(`#item-${index}`))
              // }
 
              
              // div.innerHTML = `<img class="images-loader" id="${file.name}" draggable="true" src="${src}" alt="${file.name}">`;
            
            // })            
          })
          .catch(e=>{
            console.error(e);
          })

       })

        function readfile(file,cb){
          let picReader = new FileReader;
          picReader.onloadend = cb;
          picReader.readAsDataURL(file);
        }



      })


    }



  } else {
    console.log("Your browser does not support File API");
  }

} //fin onload   /blacklist


var form = document.getElementById('formulario');

form.addEventListener('submit',function(e){
  e.preventDefault();
  let formData =  new FormData(form);
  
  console.log(fileList);

  fileList.forEach(function (file) {
    formData.append('files', file);
  });



  sendData(formData);
})

let sendData = function (data) {
  var request = new XMLHttpRequest();
  request.open("POST", '/guardar');
  request.send(data);
};

/**
 * 
 * @param {*} array 
 * @param {*} callback 
 * 
 */

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}
